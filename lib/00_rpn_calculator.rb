class RPNCalculator
  # TODO: your code goes here!
  def initialize
    @stack = []
  end

  def push(n)
    @stack << n
  end

  def plus
    perform_op(:+)
  end

  def value
    @stack.last
  end

  def minus
    perform_op(:-)
  end

  def divide
    perform_op(:/)
  end

  def times
    perform_op(:*)
  end

  def tokens(str)
    tokens = str.split
    tokens.map { |ch| is_operation?(ch) ? ch.to_sym : Integer(ch)}
  end

  def evaluate(str)
    tokens = tokens(str)
    tokens.each do |token|
      case token
      when Integer
        push(token)
      else
        perform_op(token)
      end
    end
    value
  end



  private

  def is_operation?(char)
    [:+, :-, :*, :/].include?(char.to_sym)
  end

  def perform_op(opsym)

    raise "calculator is empty" if @stack.size < 2

    f2 = @stack.pop
    f1 = @stack.pop

    case opsym
    when :+
      @stack << f1 + f2
    when :-
      @stack << f1 - f2
    when :*
      @stack << f1 * f2
    when :/
      @stack << f1.fdiv(f2)
    else
      @stack << f1
      @stack << f2
      raise "No such operation: #{opsym}"
    end
  end

end
